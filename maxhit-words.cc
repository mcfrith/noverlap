// Author: Martin C. Frith 2021
// SPDX-License-Identifier: MIT

#include <getopt.h>

#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdlib.h>

#include <iostream>
#include <random>
#include <sstream>
#include <stdexcept>
#include <vector>

typedef unsigned long ulong;

static unsigned reverseComplement(unsigned wordLen, unsigned wordNum) {
  unsigned x = ~wordNum;
  unsigned y = 0;
  for (unsigned i = 0; i < wordLen; ++i) {
    unsigned j = wordLen - 1 - i;
    y |= ((x >> i) & 1) << j;
  }
  return y;
}

static double doubleFromString(const char *s) {
  char *e;
  double x = strtod(s, &e);
  if (e == s) throw std::runtime_error("bad number");
  return x;  // xxx check overflow?
}

static unsigned numFromString(const char *s) {
  std::istringstream iss(s);
  unsigned x;
  iss >> x;
  if (!iss) throw std::runtime_error("bad integer");
  return x;
}

static unsigned positiveInt(const char *s) {
  unsigned x = numFromString(s);
  if (x < 1) throw std::runtime_error("invalid positive int value");
  return x;
}

static void checkLengths(unsigned seqLen, unsigned wordLen) {
  if (seqLen < wordLen) {
    throw std::runtime_error("sequence length must be >= word length");
  }
  if (seqLen > sizeof(ulong) * CHAR_BIT) {
    throw std::runtime_error("sequence length too big");
  }
  if (wordLen >= sizeof(unsigned) * CHAR_BIT - 1) {
    throw std::runtime_error("word length too big");
  }
}

static ulong lossFunc(ulong *dpMem, const unsigned *wordNums,
		      unsigned numOfWords, unsigned numOfAllWords,
		      unsigned dpLen) {
  const unsigned half = numOfAllWords >> 1;
  ulong *dpOld = dpMem;
  ulong *dpNew = dpMem + numOfAllWords;

  for (unsigned j = 0; j < numOfAllWords; ++j) {
    dpNew[j] = 1;
  }
  for (unsigned k = 0; k < numOfWords; ++k) {
    dpNew[wordNums[k]] = 0;
  }

  for (unsigned i = 0; i < dpLen; ++i) {
    std::swap(dpOld, dpNew);
    for (unsigned j = 0; j < half; ++j) {
      dpNew[j * 2 + 1] = dpNew[j * 2] = dpOld[j] + dpOld[j + half];
    }
    for (unsigned k = 0; k < numOfWords; ++k) {
      dpNew[wordNums[k]] = 0;
    }
  }

  ulong numOfMissedSequences = 0;
  for (unsigned j = 0; j < numOfAllWords; ++j) {
    numOfMissedSequences += dpNew[j];
  }

  return numOfMissedSequences;
}

static double startingWeight(unsigned dpLen, double drop) {
  return pow(2 * std::min(1.0, drop), 1.0 * dpLen);
}

static double avgLossFunc(ulong *dpMem, const unsigned *wordNums,
			  int numOfWords, int numOfAllWords,
			  int dpLen, double drop) {
  const int half = numOfAllWords >> 1;
  ulong *dpOld = dpMem;
  ulong *dpNew = dpMem + numOfAllWords;
  double sumOfWeightedMissProbs = 0;
  double weight = startingWeight(dpLen, drop);

  for (int j = 0; j < numOfAllWords; ++j) {
    dpNew[j] = 1;
  }
  for (int k = 0; k < numOfWords; ++k) {
    dpNew[wordNums[k]] = 0;
  }

  for (int i = 0; i < dpLen; ++i) {
    std::swap(dpOld, dpNew);
    ulong numOfMissedSequences = 0;
    for (int j = 0; j < half; ++j) {
      ulong s = dpOld[j] + dpOld[j + half];
      numOfMissedSequences += s;
      dpNew[j * 2 + 1] = dpNew[j * 2] = s;
    }
    sumOfWeightedMissProbs += numOfMissedSequences * weight;
    weight /= 2 * drop;
    for (int k = 0; k < numOfWords; ++k) {
      dpNew[wordNums[k]] = 0;
    }
  }

  ulong numOfMissedSequences = 0;
  for (int j = 0; j < numOfAllWords; ++j) {
    numOfMissedSequences += dpNew[j];
  }
  sumOfWeightedMissProbs += numOfMissedSequences * weight;

  return sumOfWeightedMissProbs;
}

static double sumOfWeights(unsigned seqLen, unsigned dpLen, double drop) {
  double n = dpLen + 1;
  double x = pow(2.0, 1.0 * seqLen);
  if (drop > 1)
    return x * (1 - pow(1 / drop, n)) / (1 - 1 / drop);
  if (drop < 1)
    return x * (1 - pow(drop, n)) / (1 - drop);
  return x * n;
}

static void printWords(unsigned wordLen, unsigned numOfWords,
		       const unsigned *wordNums) {
  for (unsigned i = 0; i < numOfWords; ++i) {
    unsigned wordCode = wordNums[i];
    for (unsigned b = wordLen; b --> 0; ) {
      std::cout << "ry"[wordCode >> b & 1];
    }
    if (i + 1 < numOfWords) std::cout << ' ';
  }
  std::cout << std::endl;
}

static void printWordsData(unsigned wordLen, unsigned numOfWords,
			   unsigned seqLen, const unsigned *wordNums,
			   ulong numOfMissedSequences) {
  double missProb = numOfMissedSequences / pow(2.0, 1.0 * seqLen);
  std::cout << numOfMissedSequences << '\t' << missProb << '\t';
  printWords(wordLen, numOfWords, wordNums);
}

static void printWordsAvgData(unsigned wordLen, unsigned numOfWords,
			      unsigned seqLen, const unsigned *wordNums,
			      double score, double drop) {
  double avgMissProb = score / sumOfWeights(seqLen, seqLen - wordLen, drop);
  std::cout << avgMissProb << '\t';
  printWords(wordLen, numOfWords, wordNums);
}

static ulong missLowerBound(unsigned numOfWords, unsigned numOfAllWords,
			    unsigned dpLen) {
  if (dpLen >= numOfAllWords / numOfWords) return 0;
  ulong x = numOfAllWords - numOfWords * (dpLen + 1);
  return x << dpLen;
}

static double avgMissLowerBound(unsigned numOfWords, unsigned numOfAllWords,
				unsigned dpLen, double drop) {
  double lowerBound = 0;
  double weight = startingWeight(dpLen, drop);
  for (unsigned i = 0; i <= dpLen; ++i) {
    ulong b = missLowerBound(numOfWords, numOfAllWords, i);
    lowerBound += b * weight;
    weight /= 2 * drop;
  }
  return lowerBound;
}

static void simulatedAnnealing(unsigned seqLen, unsigned wordLen,
			       unsigned numOfWords, unsigned numOfAllWords,
			       unsigned numOfOkWords, double drop, double cool,
			       bool isBothStrands) {
  const unsigned dpLen = seqLen - wordLen;
  const double minTemperature = 0.05;
  std::random_device randDev;
  std::default_random_engine randEng(randDev());

  std::vector<ulong> dpVector(numOfAllWords * 2);
  ulong *dpMem = &dpVector[0];

  std::vector<unsigned> wordNumVector(numOfOkWords);
  unsigned *wordNums = &wordNumVector[0];
  unsigned *restNums = wordNums + numOfWords;

  unsigned k = 0;
  for (unsigned i = 0; i < numOfAllWords; ++i) {
    unsigned j = reverseComplement(wordLen, i);
    if (i < j) {
      wordNums[k++] = i;
      wordNums[k++] = j;
    }
    if (!isBothStrands && i == j) wordNums[k++] = i;
  }
  assert(k == numOfOkWords);

  const unsigned loopMax = (numOfWords >> isBothStrands);
  const unsigned randMax = (numOfOkWords >> isBothStrands) - 1;

  for (unsigned i = 0; i < loopMax; ++i) {
    std::uniform_int_distribution<unsigned> r(i, randMax);
    unsigned j = r(randEng);
    unsigned *p = wordNums + (i << isBothStrands);
    unsigned *q = wordNums + (j << isBothStrands);
    std::swap(p[0], q[0]);
    if (isBothStrands) std::swap(p[1], q[1]);
  }

  assert(numOfOkWords > numOfWords);  // xxx

  const unsigned numOfOther = numOfOkWords - numOfWords;
  const unsigned wordRandMax = (numOfWords >> isBothStrands) - 1;
  const unsigned restRandMax = (numOfOther >> isBothStrands) - 1;
  std::uniform_int_distribution<unsigned> wordRand(0, wordRandMax);
  std::uniform_int_distribution<unsigned> restRand(0, restRandMax);
  std::uniform_real_distribution<double> unitRand;

  if (drop > 0) {
    double bound = avgMissLowerBound(numOfWords, numOfAllWords, dpLen, drop);
    double score = avgLossFunc(dpMem, wordNums, numOfWords, numOfAllWords,
			       dpLen, drop);
    double boundProb = bound / sumOfWeights(seqLen, dpLen, drop);
    std::cout << "# lower bound: " << boundProb << '\n';
    printWordsAvgData(wordLen, numOfWords, seqLen, wordNums, score, drop);
    double minScore = score;
    for (double t = score - bound; t > minTemperature; t *= cool) {
      unsigned *wordNumsPtr = wordNums + (wordRand(randEng) << isBothStrands);
      unsigned *restNumsPtr = restNums + (restRand(randEng) << isBothStrands);
      unsigned x = wordNumsPtr[0];
      unsigned y = wordNumsPtr[isBothStrands];
      wordNumsPtr[0] = restNumsPtr[0];
      wordNumsPtr[isBothStrands] = restNumsPtr[isBothStrands];
      double s = avgLossFunc(dpMem, wordNums, numOfWords, numOfAllWords,
			     dpLen, drop);
      if (s <= score || unitRand(randEng) < exp((score - s) / t)) {
	wordNumsPtr = restNumsPtr;
	score = s;
	if (score < minScore) {
	  minScore = score;
	  printWordsAvgData(wordLen, numOfWords, seqLen, wordNums, s, drop);
	}
      }
      wordNumsPtr[0] = x;
      wordNumsPtr[isBothStrands] = y;
    }
  } else {
    ulong bound = missLowerBound(numOfWords, numOfAllWords, dpLen);
    ulong score = lossFunc(dpMem, wordNums, numOfWords, numOfAllWords, dpLen);
    std::cout << "# lower bound: " << bound << '\n';
    printWordsData(wordLen, numOfWords, seqLen, wordNums, score);
    ulong minScore = score;
    for (double t = score - bound; t > minTemperature; t *= cool) {
      unsigned *wordNumsPtr = wordNums + (wordRand(randEng) << isBothStrands);
      unsigned *restNumsPtr = restNums + (restRand(randEng) << isBothStrands);
      unsigned x = wordNumsPtr[0];
      unsigned y = wordNumsPtr[isBothStrands];
      wordNumsPtr[0] = restNumsPtr[0];
      wordNumsPtr[isBothStrands] = restNumsPtr[isBothStrands];
      long a = score;
      long b = lossFunc(dpMem, wordNums, numOfWords, numOfAllWords, dpLen);
      long scoreDiff = a - b;
      if (scoreDiff >= 0 || unitRand(randEng) < exp(scoreDiff / t)) {
	wordNumsPtr = restNumsPtr;
	score -= scoreDiff;
	if (score < minScore) {
	  minScore = score;
	  printWordsData(wordLen, numOfWords, seqLen, wordNums, score);
	  if (score == bound) break;
	}
      }
      wordNumsPtr[0] = x;
      wordNumsPtr[isBothStrands] = y;
    }
  }
}

static void findBestWords(unsigned seqLen, unsigned wordLen,
			  unsigned numOfWords, unsigned numOfAllWords,
			  double drop) {
  const unsigned dpLen = seqLen - wordLen;

  std::vector<ulong> dpVector(numOfAllWords * 2);
  ulong *dpMem = &dpVector[0];

  std::vector<unsigned> wordNumVector(numOfWords);
  unsigned *stack = &wordNumVector[0];

  const unsigned wMax = numOfAllWords - numOfWords;
  ulong minLoss = -1;
  double minAvgLoss = HUGE_VAL;
  unsigned w = 0;
  unsigned stackPos = 0;

  for (;;) {
    stack[stackPos] = w;
    ++stackPos;
    if (stackPos == numOfWords) {
      if (drop > 0) {
	double avgLoss = avgLossFunc(dpMem, stack, numOfWords, numOfAllWords,
				     dpLen, drop);
	if (avgLoss <= minAvgLoss) {
	  printWordsAvgData(wordLen, numOfWords, seqLen, stack, avgLoss, drop);
	  minAvgLoss = avgLoss;
	}
      } else {
	ulong loss = lossFunc(dpMem, stack, numOfWords, numOfAllWords, dpLen);
	if (loss <= minLoss) {
	  printWordsData(wordLen, numOfWords, seqLen, stack, loss);
	  minLoss = loss;
	}
      }
      --stackPos;
    }
    while (w == wMax + stackPos) {
      if (stackPos == 0) return;
      --stackPos;
      w = stack[stackPos];
    }
    ++w;
  }
}

static void checkOneWordLength(unsigned wordLen, unsigned newLen) {
  if (wordLen > 0 && newLen != wordLen) {
    throw std::runtime_error("the words must all have the same length");
  }
}

static void evaluateWords(unsigned seqLen, const char *wordList, double drop) {
  unsigned wordLen = 0;
  std::vector<unsigned> wordNumVector;

  unsigned w = 0;
  unsigned len = 0;

  for (const char *i = wordList; *i; ++i) {
    char c = *i;
    if (c == 'r' || c == 'R') {
      ++len;
      w = w * 2;
    } else if (c == 'y' || c == 'Y') {
      ++len;
      w = w * 2 + 1;
    } else if (len > 0) {
      checkOneWordLength(wordLen, len);
      wordLen = len;
      wordNumVector.push_back(w);
      w = 0;
      len = 0;
    }
  }

  if (len > 0) {
    checkOneWordLength(wordLen, len);
    wordLen = len;
    wordNumVector.push_back(w);
  }

  unsigned numOfWords = wordNumVector.size();
  if (numOfWords == 0) throw std::runtime_error("bad input");
  checkLengths(seqLen, wordLen);
  unsigned numOfAllWords = 1 << wordLen;
  unsigned dpLen = seqLen - wordLen;
  unsigned *wordNums = &wordNumVector[0];

  std::vector<ulong> dpVector(numOfAllWords * 2);
  ulong *dpMem = &dpVector[0];

  for (unsigned i = 0; i <= dpLen; ++i) {
    ulong s = lossFunc(dpMem, wordNums, numOfWords, numOfAllWords, i);
    double missProb = s / pow(2.0, 1.0 * (wordLen + i));
    std::cout << (wordLen + i) << '\t' << s << '\t' << missProb << '\n';
  }

  if (drop > 0) {
    double s = avgLossFunc(dpMem, wordNums, numOfWords, numOfAllWords, dpLen,
			   drop);
    double avgProb = s / sumOfWeights(seqLen, dpLen, drop);
    std::cout << "weighted avg\t" << avgProb << std::endl;
  }
}

static void run(int argc, char **argv) {
  bool isBothStrands = false;
  double cool = 0;
  double drop = 0;

  const char help[] = "\
usage: maxhit-words sequence_length word_length number_of_words\n\
   or: maxhit-words sequence_length list,of,words,to,evaluate\n\
\n\
Find a set of 'ry' words, with maximum probability that at least 1 of them\n\
occurs in a random sequence.\n\
\n\
optional arguments:\n\
  -h, --help            show this help message and exit\n\
  -b, --both            if a word is used, so is its reverse-complement, and\n\
                        no word that equals its reverse-complement is used\n\
  -c COOL, --cool COOL  do simulated annealing, and multiply the temperature\n\
                        by this amount each step\n\
  -d D, --drop D        use weighted average probability over sequence lengths,\n\
                        with D times less weight for each increment in length\n\
";

  const char sOpts[] = "hbc:d:";

  static struct option lOpts[] = {
    { "help", no_argument,       0, 'h' },
    { "both", no_argument,       0, 'b' },
    { "cool", required_argument, 0, 'c' },
    { "drop", required_argument, 0, 'd' },
    { 0, 0, 0, 0 }
  };

  int c;
  while ((c = getopt_long(argc, argv, sOpts, lOpts, &c)) != -1) {
    switch (c) {
    case 'h':
      std::cout << help;
      return;
    case 'b':
      isBothStrands = true;
      break;
    case 'c':
      cool = doubleFromString(optarg);
      break;
    case 'd':
      drop = doubleFromString(optarg);
      break;
    case '?':
      std::cerr << help;
      throw std::runtime_error("");
    }
  }

  std::cout.precision(17);

  if (argc - optind == 2) {
    unsigned seqLen = positiveInt(argv[optind]);
    const char *wordList = argv[optind + 1];
    evaluateWords(seqLen, wordList, drop);
  } else if (argc - optind == 3) {
    unsigned seqLen = positiveInt(argv[optind]);
    unsigned wordLen = positiveInt(argv[optind + 1]);
    unsigned numOfWords = positiveInt(argv[optind + 2]);
    checkLengths(seqLen, wordLen);
    if (isBothStrands && numOfWords % 2) {
      throw std::runtime_error("the number of words should be even");
    }
    unsigned numOfAllWords = 1 << wordLen;
    unsigned numOfOkWords = numOfAllWords;
    if (isBothStrands && wordLen % 2 == 0) {
      numOfOkWords -= 1 << (wordLen / 2);  // palindrome words are not ok
    }
    if (numOfWords > numOfOkWords) throw std::runtime_error("too many words");
    std::cout << '#';
    for (int i = 0; i < argc; ++i) std::cout << ' ' << argv[i];
    std::cout << '\n';
    if (cool > 0) {
      simulatedAnnealing(seqLen, wordLen, numOfWords, numOfAllWords,
			 numOfOkWords, drop, cool, isBothStrands);
    } else {
      if (isBothStrands) {
	throw std::runtime_error("for -b/--both, use simulated annealing");
      }
      findBestWords(seqLen, wordLen, numOfWords, numOfAllWords, drop);
    }
  } else {
    std::cerr << help;
    throw std::runtime_error("");
  }
}

int main(int argc, char **argv) {
  try {
    run(argc, argv);
    return EXIT_SUCCESS;
  } catch (const std::exception &e) {
    const char *s = e.what();
    if (*s) std::cerr << argv[0] << ": " << s << '\n';
    return EXIT_FAILURE;
  }
}
