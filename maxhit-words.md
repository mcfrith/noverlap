# maxhit-words

You give it a sequence length `s`, word length `k`, and number of
words `n`.  It finds a set of words, such that the maximum number of
length-`s` sequences contain at least one of the words.

E.g. this finds four length-5 words:

    maxhit-words 11 5 4

The sequences and words are made of a binary r/y (purine/pyrimidine)
alphabet, and the output looks like this:

    496     0.2421875       rrrry yrrry yrryy yryyy

This means that 496 out of all possible length-11 sequences (a
fraction 0.2421875) do not contain at least one of `rrrry`, `yrrry`,
`yrryy`, or `yryyy`.

By default it guarantees to find an optimal result, but this can be
extremely slow.  Instead, you can use [simulated annealing][], which
tries to find an optimum, but does not guarantee it:

    maxhit-words -c0.99999 12 6 8

This makes it multiply the annealing "temperature" by 0.99999 at each
step: the closer this is to 1, the more slowly and thoroughly it
searches.  Simulated annealing uses random choices, so the result may
be different each time.

## Evaluating a set of words

You can give it a set of words (separated by spaces and/or commas or
other punctuation) to evaluate:

    maxhit-words 11 "rrrry ryryy yrrry yrryy"

This will test all sequence lengths between 5 (the word length) and
11:

    5     28    0.875
    6     48    0.75
    7     80    0.625
    8     130   0.5078125
    9     208   0.40625
    10    328   0.3203125
    11    512   0.25

This means that, for example, 80 out of all possible length-7
sequences (a fraction 0.625) have none of those words.

## Weighted average over sequence lengths

What if we want words with high probability of occurring in sequences
of various lengths?  The `-d1` option seeks a set of words with
maximum average probability of occurring in sequences of length >= `k`
and <= `s`:

    maxhit-words -d1 22 5 4

The output shows the average probability that none of the words occur.
You can weight it towards shorter sequences, e.g. `-d4` maximizes the
weighted average probability, where the weight decreases four-fold for
each increment in sequence length:

    maxhit-words -d4 22 5 4

Or you can weight it towards longer sequences:

    maxhit-words -d0.25 22 5 4

[simulated annealing]: https://en.wikipedia.org/wiki/Simulated_annealing
