// Author: Martin C. Frith 2020
// SPDX-License-Identifier: MIT

#include <getopt.h>

#include <math.h>
#include <stdlib.h>
#include <string.h>

#include <algorithm>
#include <iostream>
#include <random>
#include <sstream>
#include <stdexcept>
#include <vector>

struct Parameters {
  const char *alphabet;
  unsigned wordLength;
  unsigned numOfWords;
  unsigned sequenceLength;
  unsigned bound;
  double cool;

  unsigned alphabetSize;
  unsigned numOfAllWords;
  unsigned minOverlap;
};

struct WordData {
  unsigned wordNum;
  unsigned score;
};

struct StackStruct {
  WordData *wordsBeg;
  WordData *wordsEnd;
  unsigned score;
};

unsigned power(unsigned x, unsigned y) {
  unsigned theMax = -1;
  if (x > 0) theMax /= x;
  unsigned z = 1;
  for (unsigned i = 0; i < y; ++i) {
    if (z > theMax) throw std::runtime_error("too-big numbers");
    z *= x;
  }
  return z;
}

double doubleFromString(const char *s) {
  char *e;
  double x = strtod(s, &e);
  if (e == s) throw std::runtime_error("bad number");
  return x;  // xxx check overflow?
}

unsigned numFromString(const char *s) {
  std::istringstream iss(s);
  unsigned x;
  iss >> x;
  if (!iss) throw std::runtime_error("bad integer");
  return x;
}

unsigned positiveInt(const char *s) {
  unsigned x = numFromString(s);
  if (x < 1) throw std::runtime_error("invalid positive int value");
  return x;
}

unsigned overlapScore(const Parameters &p,
		      const char *word1, const char *word2) {
  const unsigned alphabetSize = p.alphabetSize;
  const unsigned wordLength = p.wordLength;
  const unsigned sequenceLength = p.sequenceLength;
  const unsigned minOverlap = p.minOverlap;
  unsigned score = 0;
  unsigned m = 1;
  for (unsigned i = minOverlap; i < wordLength; ++i) {
    unsigned j = wordLength - i;
    if (memcmp(word1 + j, word2, i) == 0) {
      score += sequenceLength ? (sequenceLength - wordLength + 1 - j) * m : m;
    }
    m *= alphabetSize;
  }
  return score;
}

double vmrFromScore(const Parameters &p, unsigned score) {
  const unsigned wordLength = p.wordLength;
  const unsigned numOfWords = p.numOfWords;
  const unsigned sequenceLength = p.sequenceLength;
  const unsigned alphabetSize = p.alphabetSize;
  const unsigned numOfAllWords = p.numOfAllWords;
  const unsigned minOverlap = p.minOverlap;
  unsigned denom = numOfWords * numOfAllWords;
  unsigned z = 2 * wordLength - 1;
  if (sequenceLength) {
    unsigned numOfPositions = sequenceLength - wordLength + 1;
    denom *= numOfPositions;
    if (minOverlap > 1) {
      z = numOfPositions * numOfPositions;
    } else {
      z = z * sequenceLength - (3 * wordLength - 1) * (wordLength - 1);
    }
  }
  unsigned constPart = denom - z * numOfWords * numOfWords;
  return
    1.0 * (constPart + 2 * power(alphabetSize, minOverlap) * score) / denom;
}

void printWords(const Parameters &p, const char *allWords,
		unsigned *wordNums, unsigned score) {
  const unsigned wordLength = p.wordLength;
  const unsigned numOfWords = p.numOfWords;
  std::sort(wordNums, wordNums + numOfWords);
  std::cout << score << '\t';
  std::cout << vmrFromScore(p, score) << '\t';
  for (unsigned i = 0; i < numOfWords; ++i) {
    std::cout.write(allWords + wordNums[i] * wordLength, wordLength);
    if (i + 1 < numOfWords) std::cout << ' ';
  }
  std::cout << std::endl;
}

void simulatedAnnealing(const Parameters &args, const char *allWords,
			const unsigned *vec, const unsigned *mat) {
  const unsigned numOfWords = args.numOfWords;
  const unsigned numOfAllWords = args.numOfAllWords;
  const double cool = args.cool;
  const double minTemperature = 0.05;
  std::random_device randDev;
  std::default_random_engine randEng(randDev());

  std::vector<unsigned> wordNumVector(numOfAllWords);
  unsigned *wordNums = &wordNumVector[0];
  unsigned *restNums = wordNums + numOfWords;

  for (unsigned i = 0; i < numOfAllWords; ++i) wordNums[i] = i;

  unsigned score = 0;

  for (unsigned i = 0; i < numOfWords; ++i) {
    std::uniform_int_distribution<unsigned> r(i, numOfAllWords - 1);
    unsigned j = r(randEng);
    std::swap(wordNums[i], wordNums[j]);
    const unsigned *row = mat + wordNums[i] * numOfAllWords;
    for (unsigned k = 0; k <= i; ++k) score += row[wordNums[k]];
  }

  printWords(args, allWords, wordNums, score);

  const unsigned numOfOther = numOfAllWords - numOfWords;
  if (numOfOther == 0) return;
  std::uniform_int_distribution<unsigned> wordRand(0, numOfWords - 1);
  std::uniform_int_distribution<unsigned> restRand(0, numOfOther - 1);
  std::uniform_real_distribution<double> unitRand;
  unsigned minScore = score;

  for (double t = score; t > minTemperature; t *= cool) {
    unsigned wordNumsIdx = wordRand(randEng);
    unsigned x = wordNums[wordNumsIdx];
    const unsigned *xRow = mat + x * numOfAllWords;

    unsigned restNumsIdx = restRand(randEng);
    unsigned y = restNums[restNumsIdx];
    const unsigned *yRow = mat + y * numOfAllWords;

    int xVal = yRow[x];
    int yVal = vec[y];
    int scoreDiff = xVal - yVal;
    for (unsigned i = 0; i < numOfWords; ++i) {
      int xCell = xRow[wordNums[i]];
      int yCell = yRow[wordNums[i]];
      scoreDiff += xCell - yCell;
    }

    if (scoreDiff >= 0 || unitRand(randEng) < exp(scoreDiff / t)) {
      wordNums[wordNumsIdx] = y;
      restNums[restNumsIdx] = x;
      score -= scoreDiff;
      if (score < minScore) {
	minScore = score;
	printWords(args, allWords, wordNums, score);
	if (score == 0) break;
      }
    }
  }
}

void findMinimalVarianceWords(const Parameters &args) {
  const unsigned wordLength = args.wordLength;
  const unsigned numOfWords = args.numOfWords;
  const unsigned alphabetSize = args.alphabetSize;
  const unsigned numOfAllWords = args.numOfAllWords;

  std::vector<char> allWordsVector(numOfAllWords * wordLength);
  char *allWords = &allWordsVector[0];
  for (unsigned wordNum = 0; wordNum < numOfAllWords; ++wordNum) {
    unsigned w = wordNum;
    for (unsigned i = 0; i < wordLength; ++i) {
      unsigned j = wordLength - 1 - i;
      char base = args.alphabet[w % alphabetSize];
      allWords[wordNum * wordLength + j] = base;
      w /= alphabetSize;
    }
  }

  std::vector<unsigned> uintVector(numOfWords + numOfAllWords +
				   numOfAllWords * numOfAllWords);
  unsigned *wordNums = &uintVector[0];
  unsigned *vec = wordNums + numOfWords;
  unsigned *mat = vec + numOfAllWords;

  for (unsigned i = 0; i < numOfAllWords; ++i) {
    const char *w = allWords + i * wordLength;
    vec[i] = overlapScore(args, w, w);
  }

  for (unsigned i = 0; i < numOfAllWords; ++i) {
    for (unsigned j = 0; j < numOfAllWords; ++j) {
      const char *v = allWords + i * wordLength;
      const char *w = allWords + j * wordLength;
      mat[i * numOfAllWords + j] =
	overlapScore(args, v, w) + overlapScore(args, w, v);
    }
    mat[i * numOfAllWords + i] /= 2;
  }

  std::cout << "#score\tVMR\twords" << std::endl;

  if (args.cool > 0) {
    simulatedAnnealing(args, allWords, vec, mat);
    return;
  }

  std::vector<WordData> wordDataVector(numOfAllWords * numOfWords);
  WordData *wordData = &wordDataVector[0];

  std::vector<StackStruct> stackVector(numOfWords);
  StackStruct *stack = &stackVector[0];

  unsigned greedyScore = 0;
  // greedy search for a set of words with minimum score:
  for (unsigned i = 0; i < numOfWords; ++i) {
    unsigned minScorePart = -1;
    unsigned bestWordNum = -1;
    for (unsigned j = 0; j < numOfAllWords; ++j) {
      const unsigned *row = mat + j * numOfAllWords;
      unsigned scorePart = vec[j];
      for (unsigned k = 0; k < i; ++k) {
	unsigned wordNum = wordNums[k];
	if (wordNum == j) {
	  scorePart = minScorePart;
	  break;
	}
	scorePart += row[wordNum];
      }
      if (scorePart < minScorePart) {
	minScorePart = scorePart;
	bestWordNum = j;
      }
    }
    wordNums[i] = bestWordNum;
    greedyScore += minScorePart;
  }

  unsigned scoreLimit = args.bound;
  if (greedyScore < scoreLimit) {
    printWords(args, allWords, wordNums, greedyScore);
    scoreLimit = greedyScore;
  }

  unsigned stackPos = 0;
  unsigned oldScore = 0;
  unsigned minNewWords = numOfWords - 1;
  WordData *oldWordsBeg = wordData;
  WordData *oldWordsEnd = oldWordsBeg;

  for (unsigned i = 0; i < numOfAllWords; ++i) {
    unsigned newScore = vec[i];
    if (newScore < scoreLimit) {
      oldWordsEnd->wordNum = i;
      oldWordsEnd->score = newScore;
      ++oldWordsEnd;
    }
  }

  while (1) {
    while (oldWordsEnd - oldWordsBeg > minNewWords) {
      const unsigned *row = mat + numOfAllWords * oldWordsBeg->wordNum;
      unsigned score = oldScore + oldWordsBeg->score;
      ++oldWordsBeg;
      if (score >= scoreLimit) continue;
      unsigned scorePartLimit = scoreLimit - score;
      if (minNewWords > 1) {
	unsigned needOne = scorePartLimit / minNewWords;
	WordData *newWordsEnd = oldWordsEnd;
	for (WordData *i = oldWordsBeg; i < oldWordsEnd; ++i) {
	  unsigned scorePart = i->score + row[i->wordNum];
	  if (scorePart < scorePartLimit) {
	    newWordsEnd->wordNum = i->wordNum;
            newWordsEnd->score = scorePart;
            ++newWordsEnd;
            if (scorePart <= needOne) needOne = -1;  // found
	  }
	}
	if (newWordsEnd - oldWordsEnd >= minNewWords && needOne + 1 == 0) {
	  stack[stackPos].score = oldScore;
	  stack[stackPos].wordsBeg = oldWordsBeg;
	  stack[stackPos].wordsEnd = oldWordsEnd;
	  ++stackPos;  --minNewWords;
	  oldScore = score;
	  oldWordsBeg = oldWordsEnd;
	  oldWordsEnd = newWordsEnd;
	}
      } else {
	for (WordData *i = oldWordsBeg; i < oldWordsEnd; ++i) {
	  unsigned scorePart = i->score + row[i->wordNum];
	  if (scorePart < scorePartLimit) {
	    unsigned newScore = score + scorePart;
            stack[stackPos].wordsBeg = oldWordsBeg;
            stack[stackPos + 1].wordsBeg = i + 1;
	    for (unsigned i = 0; i < numOfWords; ++i) {
	      wordNums[i] = stack[i].wordsBeg[-1].wordNum;
	    }
            printWords(args, allWords, wordNums, newScore);
            scorePartLimit = scorePart;
            scoreLimit = newScore;
	  }
	}
      }
    }
    if (stackPos == 0) break;
    --stackPos;  ++minNewWords;
    oldScore = stack[stackPos].score;
    oldWordsBeg = stack[stackPos].wordsBeg;
    oldWordsEnd = stack[stackPos].wordsEnd;
  }
}

void run(int argc, char **argv) {
  Parameters args;
  args.alphabet = "acgt";
  args.sequenceLength = -1;
  args.bound = -1;
  args.cool = 0;

  std::ostringstream help;
  help << "\
usage: " << argv[0] << " [-h] [-a A] [-s S] [-c COOL] k n\n\
\n\
Find n words with minimum variance in random sequences.\n\
\n\
positional arguments:\n\
  k                     word length\n\
  n                     number of words\n\
\n\
optional arguments:\n\
  -h, --help            show this help message and exit\n\
  -a A, --alphabet A    alphabet (default: " << args.alphabet << ")\n\
  -s S, --sequence-length S\n\
                        sequence length, 0=infinite/circular (default: 2k-1)\n\
  -b B, --bound B       only find words with score < B (faster)\n\
  -c COOL, --cool COOL  do simulated annealing, and multiply the temperature\n\
                        by this amount each step\n\
";

  const char sOpts[] = "ha:s:b:c:";

  static struct option lOpts[] = {
    { "help",            no_argument,       0, 'h' },
    { "alphabet",        required_argument, 0, 'a' },
    { "sequence-length", required_argument, 0, 's' },
    { "bound",           required_argument, 0, 'b' },
    { "cool",            required_argument, 0, 'c' },
    { 0, 0, 0, 0 }
  };

  int c;
  while ((c = getopt_long(argc, argv, sOpts, lOpts, &c)) != -1) {
    switch (c) {
    case 'h':
      std::cout << help.str();
      return;
    case 'a':
      args.alphabet = optarg;
      break;
    case 's':
      args.sequenceLength = numFromString(optarg);
      break;
    case 'b':
      args.bound = numFromString(optarg);
      break;
    case 'c':
      args.cool = doubleFromString(optarg);
      break;
    case '?':
      std::cerr << help.str();
      throw std::runtime_error("");
    }
  }

  if (argc - optind == 2) {
    args.wordLength = positiveInt(argv[optind]);
    args.numOfWords = positiveInt(argv[optind + 1]);
    args.alphabetSize = strlen(args.alphabet);
    args.numOfAllWords = power(args.alphabetSize, args.wordLength);
    if (args.numOfWords > args.numOfAllWords) {
      throw std::runtime_error("too many words");
    }
    if (args.sequenceLength + 1 == 0) {
      args.sequenceLength = 2 * args.wordLength - 1;
    }
    if (args.sequenceLength > 0 && args.sequenceLength < args.wordLength) {
      throw std::runtime_error("the sequence length must be >= k");
    }
    args.minOverlap = 1;
    if (args.sequenceLength > 0 && args.sequenceLength < 2 * args.wordLength) {
      args.minOverlap = 2 * args.wordLength - args.sequenceLength;
    }
    std::cout << '#';
    for (int i = 0; i < argc; ++i) std::cout << ' ' << argv[i];
    std::cout << '\n';
    findMinimalVarianceWords(args);
  } else {
    std::cerr << help.str();
    throw std::runtime_error("");
  }
}

int main(int argc, char **argv) {
  try {
    run(argc, argv);
    return EXIT_SUCCESS;
  } catch (const std::exception &e) {
    const char *s = e.what();
    if (*s) std::cerr << argv[0] << ": " << s << '\n';
    return EXIT_FAILURE;
  }
}
