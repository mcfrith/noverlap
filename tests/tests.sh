#! /bin/sh

cd $(dirname $0)

PATH=../bin:$PATH

{
    noverlap-clique --help
    noverlap-clique 3
    noverlap-clique -a ry 5
    noverlap-clique -a ry -g1 5
    noverlap-clique -s2 3
    noverlap-clique -ary -r1 8
    noverlap-clique -ary -r2 10

    minvar-words --help
    minvar-words 2 5
    minvar-words -s0 2 5
    minvar-words -s0 -a ry 5 4
    minvar-words -s9 -a ry 5 4
    minvar-words -s0 ac,at,gc,gt
    minvar-words -s0 rryny

    minvar-words-fast --help
    minvar-words-fast 2 5
    minvar-words-fast -s0 2 5
    minvar-words-fast -s0 -a ry 5 4
    minvar-words-fast -s9 -a ry 5 4

    maxhit-words 7 4 4
    maxhit-words -d1 10 4 4
    maxhit-words 8 rrrry,rryrr,ryryr,ryyrr,ryyry,ryyyy,yyyrr,yyyry
    maxhit-words -d1 8 rrrry,rryrr,ryryr,ryyrr,ryyry,ryyyy,yyyrr,yyyry
    maxhit-words -d2 8 rrrry,rryrr,ryryr,ryyrr,ryyry,ryyyy,yyyrr,yyyry
    maxhit-words -d0.5 8 rrrry,rryrr,ryryr,ryyrr,ryyry,ryyyy,yyyrr,yyyry

    ry-seeds --help

    dna-seed-sim --help
    dna-seed-sim -r7 -L1000
    dna-seed-sim -r7 -s4 -L1000
    dna-seed-sim -r7 -S4 -L1000
    dna-seed-sim -r7 -w A -L1000
    dna-seed-sim -r7 -w ac,at,gc,gt -L1000
    dna-seed-sim -r7 -m6 -M10 -w ry -L1000
    dna-seed-sim -r7 -u10 -L1000
    dna-seed-sim -r7 -u@@ -L1000
    dna-seed-sim -r7 -p50 -t20 -L100
    dna-seed-sim -r7 -p20 -k4 -t20 -L100
    dna-seed-sim -r7 --min-window 5 -L1000
    dna-seed-sim -r7 --min-window 15 --min-order 1 --min-type 1 -p10 -L50
    dna-seed-sim -r7 --min-window 5 --min-order 2 --min-type 2 -L2000
    dna-seed-sim -r7 --min-window 5 --min-order 3 -L2000
    dna-seed-sim -r7 --min-window 5 --min-order 4 -L2000
    dna-seed-sim -r7 --min-window 5 --min-order 4 --min-type 2 -L2000
    dna-seed-sim -r7 -M13 --syn-window 8 --syn-offset 4 -L5000
    dna-seed-sim -r7 -M13 --syn-window 8 --syn-sample 2 -L5000
    dna-seed-sim -r7 -M5 -wry -L1000 -d
    dna-seed-sim -r7 -a128 -m2 -M3 --min-window 7 --min-order 4 -L10000
    dna-seed-sim -r7 -m35 -M35 --syn-window=32 -L1000 --distances
    dna-seed-sim -M5 -wry chrM.fa
    dna-seed-sim -M5 -wabbbb --seeds chrM.fa
} | diff -u tests.out -
