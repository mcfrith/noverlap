# noverlap-clique

This program finds a maximum-size set of non-overlapping words, by
brute-force clique search.

Actually, it just does pre- and post-processing.  It delegates the
clique search to: MaxCliquePara, MaxCliqueDyn, or Parallel Maximum
Clique (PMC).

For example, this gets a maximum-size set of non-overlapping length-3
DNA words, using MaxCliquePara:

    noverlap-clique 3 > graph3.txt
    maxClique graph3.txt > clique3.txt
    noverlap-clique 3 clique3.txt

**Caution**!  These `noverlap-clique` settings must be the same both
times: word length, `-a`, `-r`, `-s`.

You can replace the middle step with MaxCliqueDyn:

    mcqd graph3.txt > clique3.txt

PMC requires a different graph format:

    noverlap-clique -g1 3 > graph3.mtx
    pmc -f graph3.mtx -a 0 > clique3.txt
    noverlap-clique 3 clique3.txt

This shows all options and their default settings:

    noverlap-clique --help
