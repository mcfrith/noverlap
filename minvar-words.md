# minvar-words

`minvar-words` finds a set of words whose number of occurrences in a
random sequence (of independent, equally-probable letters) has minimum
variance.

For example, this finds a minimum-variance set of ten length-3 DNA
words:

    minvar-words 3 10

It prints a "score" (the lower the better), VMR
(variance-to-mean-ratio), and the words.

This finds a minimum-variance set of four length-5 words, using a
two-letter `ry` alpbabet:

    minvar-words -a ry 5 4

You can also give it a list of words, in which case it will calculate
the VMR:

    minvar-words ac,at,gc,gt

You can abbreviate this with standard ambiguous-base notation:

    minvar-words ry

By default it calculates VMRs for a random sequence of length: 2 *
(word length) - 1.  You can make it instead use a sequence length of,
say, 32:

    minvar-words -s32 3 10

As a special case, `-s0` makes it calculate VMRs for an infinite
random sequence (which are the same as the VMRs in a finite random
circular sequence).

By default, `minvar-words` does a brute-force search that guarantees
to find the minimum variance.  But that is often too slow.  Instead,
you can use a heuristic search ([simulated annealing][]) that does not
guarantee to find the minimum:

    minvar-words -c0.999999 3 16

This tells it to multiply the "temperature" by 0.999999 at each step.
The closer this cooling factor is to 1, the more thoroughly and slowly
it will search.

[simulated annealing]: https://en.wikipedia.org/wiki/Simulated_annealing

This shows all options and their default settings:

    minvar-words --help

## minvar-words-fast

This can do some of the same things, much faster.  To compile it, type
`make`.  To see its options:

    minvar-words-fast --help
