# dna-seed-sim

`dna-seed-sim` tests alignment seeding methods on DNA sequences.  It
can show 4 things:

* Seed spacing - show distances between seed start positions, in one
  DNA sequence.
* Seed choices - show the seed start positions and sequences, in one
  DNA sequence.
* Specificity - count all pairs of matching seed positions, between two
  unrelated DNA sequences.
* Sensitivity - generate pairs of DNA sequences that differ by
  substitutions, and count the number of sequence pairs with >= 1 seed
  match at homologous positions.

## Examples

This generates two random DNA sequences of length 10000 each, and
counts pairs of (forward-strand) exact matches between them that start
with "a".  It tests match lengths between 5 and 10 (inclusive):

    dna-seed-sim -wa -m5 -M10 -L10000

This does the same, for the two sequences in the files `seq1.fasta`
and `seq2.fasta`.  It first reverses (but does not complement) seq2,
to eliminate any homology between the sequences:

    dna-seed-sim -wa -m5 -M10 seq1.fasta seq2.fasta

This gets distances between seeds starting with `ry` (purine followed
by pyrimidine), in `mySeq.fasta`:

    dna-seed-sim -wry mySeq.fasta

This does the same, for a random sequence of length 1000:

    dna-seed-sim -wry -L1000 --distances

This shows the seed sequences and their (zero-based) start
coordinates, in `mySeq.fasta`:

    dna-seed-sim -wry --seeds mySeq.fasta

This generates 1000 pairs of length-100 sequences, where each pair
differs by random substitutions with rate PAM 50, and counts the
number of sequence pairs with >= 1 seed match at homologous positions:

    dna-seed-sim -t1000 -L100 -p50 -wa -m5 -M10

## Options

To see all the options, and their default values:

    dna-seed-sim --help

### Sequence simulation

For simulated sequences, you can specify the `gc`-content (% `g`+`c`).

For simulated sequence pairs, it uses the [T92][] model of DNA
evolution.  You can specify: PAM distance, transition/transversion
rate ratio "kappa", and `gc`-content.

### Sequence files

A sequence file can be in either fasta or headerless "raw" format.
Symbols other than acgt (case-insensitive) are omitted.  If a fasta
file has more than one sequence, only the first will be used.

### Seeding options

You can select these seeding options:

* Sparse seeding, using only seeds that start at every *N*th position
  in one of the sequences, but starting at every position in the other
  sequence.  There are 2 slightly different variants:

  - Every *N*th position starting from the first position in the
    sequence.

  - Every *N*th position starting from one of the first *N* positions,
    chosen randomly.  So the average fraction of selected seed
    positions is precisely 1/*N*.

* Word-restricted seeding (yaay!)  For example, this only uses seeds
  starting at positions (in both sequences) of `ac`, `at`, `gc`, or
  `gt`:

      -w ac,at,gc,gt

  You can abbreviate this with standard ambiguous-base notation:

      -w ry

  The words given to this `-w` option are case-insensitive.

* Seed patterns.  A pattern specifies a seed length, and which
  positions allow mismatches or require certain letters.  For example:

      -P ARYryn@N

  This specifies seed length 8.  The 1st position must be `A`.
  Position 2 must be a puRine (`A` or `G`), and it must be the same
  letter in both sequences.  Position 3 must be the same pYrimidine in
  both sequences.  Position 4 must be a purine (not necessarily the
  same one) in both sequences.  Position 5 must be a pyrimidine (not
  necessarily the same one) in both sequences.  Position 6 can be any
  letter in either sequence.  Position 7 can be any letter, but must
  be a match or transition.  Position 8 can be any letter, but must be
  the same letter in both sequences.

  Synonyms:  
  `1` = `#` = `N` (exact-match position)  
  `0` = `-` = `n` (wildcard/joker/don't-care position)

  You can specify comma-separated patterns, to use the union of their
  seed hits:

      -P 11R101RY1,11Y011RY1

* Pattern unit.  For example:

      -u @@

  This makes patterns by repeating the unit `L` times (minimum seed
  length <= `L` <= maximum seed length), then stripping trailing `0`s.
  So "length" really means "number of units".

* [Minimizer] seeding: use seeds starting at any position that has the
  "minimum value" in any window of *w* consecutive positions.

  - Minimizer "order" means how to compare the sequence suffix at each
    position, so as to define minimum positions.  "Alphabetic" means
    simple alphabetic order.  "cg" means c<a<t<g at odd-numbered bases
    and g<t<a<c at even-numbered bases, so that `cgcgcg...` is the
    lowest possible suffix.  "att" means a<c<g<t at the first base and
    t<g<c<a at all subsequent bases.  "abb" means a<c<g<t at the first
    base and t=g=c<a at all subsequent bases.  "murmur64" means to
    interpret the first seed-length bases of the suffix as a base-4
    number, and calculate its murmur64 hash.

  - Minimizer "type" means what to do at the sequence edges.
    "Interior" means just use length-*w* windows that wholly fit in
    the sequence.  "Mixed" means interior plus "end-minimizers" from
    windows of any length between 1 and *w* at each end of the
    sequence.  "Circular" means pretend the sequence is circular.

* [Syncmer][] seeding.  By default they are "closed" syncmers, but
  specifying any offset (including 1) will switch to "open" syncmers.
  You cannot specify "s", but instead you can specify "window length",
  which is defined to be k-s+1 (where k is the seed length).  To
  activate syncmers, specify a window length > 1.

[Minimizer]: https://doi.org/10.1093/bioinformatics/bth408
[Syncmer]: https://doi.org/10.7717/peerj.10805
[T92]: https://en.wikipedia.org/wiki/Models_of_DNA_evolution#T92_model_(Tamura_1992)
