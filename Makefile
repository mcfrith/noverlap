CXXFLAGS = -O3 -Wall

all: bin/minvar-words-fast bin/maxhit-words

bin/minvar-words-fast: minvar-words-fast.cc
	${CXX} ${CXXFLAGS} -o $@ minvar-words-fast.cc

bin/maxhit-words: maxhit-words.cc
	${CXX} ${CXXFLAGS} -o $@ maxhit-words.cc
