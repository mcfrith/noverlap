# Minimally-overlapping words for sequence similarity search

Please see: [Minimally-overlapping words for sequence similarity
search](https://doi.org/10.1101/2020.07.24.220616).
