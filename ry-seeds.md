# ry-seeds

`ry-seeds` crudely optimizes seed patterns with positions restricted
to be purine (`R`, `r`) or pyrimidine (`Y`, `y`).

It requires a suitable version of Iedera, which shuffles an input seed
pattern.  Currently, such a version is here:
https://github.com/laurentnoe/iedera/tree/hfkn_check  
This Iedera needs to be installed in your PATH.

Example usage:

    ry-seeds -s4 -k3 20 7 RRY

This seeks an optimal seed pattern of *weight* 7, with one `Y`/`y` and
two `R`/`r` positions, and up to 4 `n` positions (that allow any match
or mismatch).  It optimizes the pattern for sequences related by PAM
distance 20, and transition/transversion rate ratio kappa = 3.

Alternatively, you can evaluate a specified pattern:

    ry-seeds -k3 20 YRRN@@@@@@

This shows all options and their default settings:

    ry-seeds --help
